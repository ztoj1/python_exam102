# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import random 
import time
import sys

def count_period(time_set, start_time, end_time):    
    period=time_set-(end_time-start_time)   
    return period

def print_period(time_set, start_time, end_time):
    period=time_set-(end_time-start_time) 
    if period>0:
        print(f"Masz jeszcze {period:.0f} sekund, żeby rozwiazać wszystkie zadania")
    
    else:
        print("Jeste za wolny")
    
def print_help(file_name, n_line):    
         with open(file_name, 'r') as f:
             lines = f.readlines()
             print(lines[n_line])
             
def get_line(file_name, n_line):
    with open(file_name, 'r') as f:
        lines = f.readlines()
        return lines[n_line]

class Room_1:
    def __init__(self, Id, text, answer_a, answer_b, answer_c, answer_d, truth):
        self.Id = Id
        self.text = text
        self.answer_a = answer_a
        self.answer_b = answer_b
        self.answer_c = answer_c
        self.answer_d = answer_d
        self.truth = truth
          
    def sumup_room1(self):
        print(f"Prawidlowa odpowiedz to {self.truth}")

class Room_2:
    beltlife_p_counter=1
    beltlife_f_counter=1
    beltlife_c_counter=1
    time_counter=0
    
    def __init__(self, Id, text, your_answer, truth_answer):
        self.Id = Id
        self.text = text
        self.your_answer = your_answer
        self.truth_answer = truth_answer
        
    def set_answer(self):
        while True:
            try:
                your_answer = int(input("Podaj wynik (liczba całkowita): "))
                self.your_answer = your_answer
                break
            except ValueError:
                print("Nie prawidłowa wartosc, odpowedz musi byc liczba")
                         
    def sumup_room2(self):
        print(f"Prawidlowa odpowiedz to {self.truth_answer}")
        
    def set_beltlife(self, decision, file_name, n_line=7):       
        if decision == "p":
            print_help(file_name, n_line)
            Room_2.beltlife_p_counter -= 1  # odwołanie do pola statycznego
            self.beltlife_p_counter = Room_2.beltlife_p_counter
            Room_2.time_counter = 120  # odwołanie do pola statycznego
            self.time_counter = Room_2.time_counter 
            print_help(file_name, 13)
            time.sleep(Room_2.time_counter)       
        elif decision == "c":
            print_help(file_name, n_line+1)
            Room_2.beltlife_c_counter -= 1  # odwołanie do pola statycznego
            self.beltlife_c_counter = Room_2.beltlife_c_counter
            Room_2.time_counter = 60  # odwołanie do pola statycznego
            self.time_counter = Room_2.time_counter 
            print_help(file_name, 13)
            time.sleep(Room_2.time_counter)
        elif decision == "f":
            print_help(file_name, n_line+2)
            Room_2.beltlife_f_counter -= 1  # odwołanie do pola statycznego
            self.beltlife_f_counter = Room_2.beltlife_f_counter
            Room_2.time_counter = 30  # odwołanie do pola statycznego
            self.time_counter = Room_2.time_counter
            print_help(file_name, 13)
            time.sleep(Room_2.time_counter)
        else:
            Room_2.time_counter = 0  # odwołanie do pola statycznego
            self.time_counter = Room_2.time_counter
            print_help(file_name, 14)
               
class Life:
    life_counter = 9

    def __init__(self, life_a, life_b, life_c, life_d):
        self.life_a = life_a
        self.life_b = life_b
        self.life_c = life_c
        self.life_d = life_d
        
    def set_life():
        
        while True:
            try:
                life_a = int(input("Ile swoich żyć stawiasz na odpowiedz a: "))
                break
            except ValueError:
                print("Nie prawidlowa wartosc, odpowedz musi byc liczba")
        while True:    
            try:
                life_b = int(input("Ile swoich żyć stawiasz na odpowiedz b: "))
                break
            except ValueError:
                print("Nie prawidlowa wartosc, odpowedz musi byc liczba")
        while True:
            try:
                life_c = int(input("Ile swoich żyć stawiasz na odpowiedz c: "))
                break
            except ValueError:
                print("Nie prawidlowa wartosc, odpowedz musi byc liczba")
        while True:
            try:
                life_d = int(input("Ile swoich żyć stawiasz na odpowiedz d: "))
                break
            except ValueError:
                print("Nie prawidlowa wartosc, odpowedz musi byc liczba")
        
        return Life(life_a, life_b, life_c, life_d)
   
    def sum_life_r1(self, life_a, life_b, life_c, life_d):
        sum_life_r1=self.life_a+self.life_b+self.life_c+self.life_d
        return sum_life_r1

    def cut_life_r1(self,choice ):
        if choice=="a":
            Life.life_counter = self.life_a  # odwołanie do pola statycznego
            self.life_counter = Life.life_counter
        
        if choice=="b":  
            Life.life_counter = self.life_b  # odwołanie do pola statycznego
            self.life_counter = Life.life_counter
        
        if choice=="c":
            Life.life_counter = self.life_c  # odwołanie do pola statycznego
            self.life_counter = Life.life_counter
        
        if choice=="d":  
            Life.life_counter = self.life_d  # odwołanie do pola statycznego
            self.life_counter = Life.life_counter
        

    def cut_life_r2(self):
        Life.life_counter -= 1  # odwołanie do pola statycznego
        self.life_counter = Life.life_counter
        

    def sumup_life(self):
        if Life.life_counter>0:
            print(f"Masz jeszcze {Life.life_counter} żyć.")
        if Life.life_counter==0:
            print("To był zły wybór")

def main():
    file_help = 'C:/Users/Magda/Desktop/ZtoJ/gra-labirynt/help.txt'
    file_room2 = 'C:/Users/Magda/Desktop/ZtoJ/gra-labirynt/room2.txt'
    
    #Rozpoczynamy w ROOM1
    print_help(file_help, 0)   
    print_help(file_help, 14)
    time.sleep(20)
    print_help(file_help, 1)
    time.sleep(15)
    
    # Baza  pytan dla Room_1-> tu wersja prymitywna - bez pliku tekstowego
    q0 = Room_1(0, "Jaka jest najdłuższa rzeka swiata", "Amazonka", "Nil", "Wołga", "Jangcy", "b")
    q1 = Room_1(1, "Jaki jest najwyzszy budynek na swiecie ", "Burdż Chalifa", "Merdeka", "Shanghai Tower", "Pałac Kultury i Nauki w Warszawie", "a")
    q2 = Room_1(2, "Jaki jest największy kontynent", "Azja", "Ameryka Północna", "Ameryka Południowa", "Afryka", "a")
    q3 = Room_1(3, "Jaki jest najwyższy szczyt na ziemi", "Mont Blanck", "Mont Everest", "Mont Giewont", "Aconcagua", "b")
    q4 = Room_1(4, "Jaka jest największa wyspa Japoni ?", "Sikoku", "Kjusiu", "Hokaido", "Honsiu", "d")
    q5 = Room_1(5, "Jakie jest największe drzewo swiata ?", "Brzoza", "Baobab", "Sekwoja", "Dąb", "c")
    q6 = Room_1(6, "Jakie jest najszybszy zwierzę na lądzie ?", "Gepart", "Strus", "Pantera", "Antylopa", "a")
    q7 = Room_1(7, "Jakie jest największe jezioro na swiecie ?", "Jezioro Kaspijskie", "Jezioro Bodeńskie", "Jezioro Wiktorii", "Bajkał", "a")
    q8 = Room_1(8, "Jakie jest największe morze ?", "Północne", "Chińskie", "Arktyczne", "adriatyckie", "c")
    q9 = Room_1(9, "Jaki jest największy kraj ?", "Kanada", "USA", "Rosja", "Chiny", "c")
    q10 = Room_1(10, "Jaka jest największ planeta ?", "Mars", "Wenus", "Saturn", "Jowisz", "d")
    q11 = Room_1(11, "Największe zwierze na ziemi ?", "Płetwal błekitny", "Słoń indyjski", "Waleń szary", "Hipopotam nilowy", "a")
    q12 = Room_1(12, "Kto jest najszybszym człowiekem na swiecie ?", "Asafa Powell", "Jesse Owens", "Carl Lewis", "Usain Bolt", "d")
    q13 = Room_1(13, "Jaki jest najbardziej zaludniony kraj ?", "USA", "Chiny", "Indie", "Pakistan", "c")
    q14 = Room_1(14, "Jaki jest najwyzszy budynek na swiecie ", "Burdż Chalifa", "Merdeka", "Shanghai Tower", "Pałac Kultury i Nauki w Warszawie", "a")
    
    list_quastion = [q0, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14]
    list_quastion_sel=[]
    list_k=[]
    n_quastion_base = len(list_quastion)  # liczba pytan w bazie / Room_1
    n_quastion_r1 = 5  # # liczba zadań w Room_1 - stała wartosc
    n = 0
    
    list_k=random.sample(range(n_quastion_base),n_quastion_r1)
    
    for i in list_k:
        list_quastion_sel.append(list_quastion[i])
    #print(list_quastion_sel)

    for i in range(0, n_quastion_r1):
        if Life.life_counter > 0:
            name = list_quastion_sel[i]
            n=n+1
            print(f"PYTANIE {n} z {n_quastion_r1}" )
            time.sleep(2)
            print(name.text)
            time.sleep(3)
            print(f"Odpowiedź a: {name.answer_a}")
            print(f"Odpowiedź b: {name.answer_b}")
            print(f"Odpowiedź c: {name.answer_c}")
            print(f"Odpowiedź d: {name.answer_d}")
            print_help(file_help, 14)
            
            decision = input("Jezeli chcesz wyswietlić pomoc, wcisnij h. Jezeli wiesz co robić, wcisnij dowolny klawisz: ")
            if decision=="h":
                print_help(file_help, 1)
                time.sleep(10)
                
            print_help(file_help, 14)
            your_life = Life.set_life()
            sum_life = your_life.sum_life_r1(your_life.life_a, your_life.life_b, your_life.life_c, your_life.life_d)

            # print(Life.life_counter)
            while sum_life != Life.life_counter:
                print(f" Masz {Life.life_counter} żyć a podzieliłes {sum_life}. Zrób to ponownie")
                your_life = Life.set_life()
                sum_life = your_life.sum_life_r1(your_life.life_a, your_life.life_b, your_life.life_c, your_life.life_d)

            print_help(file_help, 14)
            your_life.cut_life_r1(name.truth)
            name.sumup_room1()
            your_life.sumup_life()
            print_help(file_help, 14)
    
    print_help(file_help, 15)
            
    if Life.life_counter < 1:
        print_help(file_help, 3)
        sys.exit() 
    
    # Przechodzimy do ROOM 2 
    time.sleep(5)         
    print_help(file_help, 4)
    your_life.sumup_life()
    print_help(file_help, 14)
    time.sleep(5)  
    print_help(file_help, 5)
    print_help(file_help, 14)
    time.sleep(15)
        
    # Baza zadań -> wersja z wykorzystaniem pliku tekstowego 
    start_time=time.time()
    #print(start_time)  
    time_room2=300 # czas gry w Room_2 - stała wartosć
    n_task_base = 13  # całkowita ilosc pytan w bazie (plik tekstowy)
    n_task_r2 = 5  # liczba zadań w Room_2 - stała wartosc
    list_task=[]
    m=0
    
    # Baza pytań -> wersja z wykorzystaniem pliku tekstowego          
            # losowanie numerow pytan (bez powtorzen) z bazy
    
    list_z=random.sample(range(n_task_base),n_task_r2) 
    #print(list_z)
             
            #wybrane pytania z bazy
    for i in list_z:
        list_task.append(get_line(file_room2, i))
    #print(list_task)  
     
            #wywolanie pytan i wprowadzanie odpowiedzi uzytkownika
    for i in range(0, n_task_r2):
        list_task_sel=list_task[i].split(";")           
        object_name_R2=Room_2(list_task_sel[0], list_task_sel[1], list_task_sel[2], list_task_sel[3])                   
        end_time_1=time.time()
        period_1=count_period(time_room2, start_time, end_time_1)
                    
        if Life.life_counter > 0 and period_1>0 :
            m=m+1
            print(f"ZADANIE {m} z {n_task_r2}" )
            time.sleep(2)
            print(object_name_R2.text)
            print_help(file_help, 14)
            time.sleep(3)
            decision = input(" Jezeli chcesz skorzystac z pomocy wsisnij [h]. Jezeli chcesz skorzystac odrazu z koła wsisnij [p / c / f ]. Wcisnij inny klawisz, jeżeli chcesz udzielić odpowiedzi: ")
            
            
            if decision=="h":
                print_help(file_help, 6)
                decision = input("Wcisnij odpowiedni przycisk: ")
                
            while  (decision=="p" and Room_2.beltlife_p_counter==0)or   (decision=="c" and Room_2.beltlife_c_counter==0) or (decision=="f" and Room_2.beltlife_f_counter==0):
                    decision = input(" Już wykorzystałes to koło, wybierz inne: ")
            print(decision)            
            #print(start_time)  #testowanie
            object_name_R2.set_beltlife(decision, file_help)
            start_time+=Room_2.time_counter          
            #print(start_time)  #testowanie                
            object_name_R2.set_answer()
                
            if int(object_name_R2.truth_answer) != int(object_name_R2.your_answer):
                your_life.cut_life_r2()
                        
            object_name_R2.sumup_room2()
            your_life.sumup_life()
            end_time_2=time.time()
            print_period(time_room2, start_time, end_time_2)
            print_help(file_help, 14)
            time.sleep(2)
            
        elif period_1<0 :
            print_help(file_help, 10)
            sys.exit() 
            
        elif Life.life_counter<1:
            print_help(file_help, 11)
            sys.exit()
            
    print_help(file_help, 12)
               
if __name__ == "__main__":
    main()
